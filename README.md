Сборка проекта:
```
python setup.py bdist_wheel
```

Запуск тестов:
```
pip install -r requirements/development.txt
pytest tests --cov package
```

Запуск подов в кластере (ручной режим):
```
kubectl apply -f deployment.yml
```

